require 'json'

require 'websocket-eventmachine-client'

class Client
  def initialize(websocket_uri, sampler, stdout = STDOUT)
    @data = sampler
    @uri = websocket_uri

    @stdout = stdout
  end

  def ws
    WebSocket::EventMachine::Client.connect(uri: @uri)
  end

  def onmessage
    ws.onmessage do |msg|
      @data.record msg.to_f
    end
  end

  def averages
    @data.averages
  end
end
