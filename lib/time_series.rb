class TimeSeries
  def initialize
    @data = []
  end

  def record(value)
    @data << [Time.now, value]
  end

  def averages
    [
      average_of_last_n_minutes(1),
      average_of_last_n_minutes(5),
      average_of_last_n_minutes(15)
    ]
  end

  private

  def average_of_last_n_minutes(minutes)
    values = []
    index = @data.length - 1
    loop do
      break unless @data[index] && @data[index][0] > Time.now - minutes * 60
      values << @data[index][1]
      index -= 1
    end
    return 0 if values.empty?
    values.sum / values.length.to_f
  end
end
