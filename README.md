WebSocket Averager
====

Thank you for taking the time to look at my WebSocket averager app.

It "subscribes to the Websocket and once a second prints the 1, 5 and 15 minute
averages of the data from the Websocket."

Had I more time, I would have liked to improve test coverage to include some
integration tests for the EventMachine definition and the "main" code in
**`start.rb`**. For the latter purpose, and as a rule of thumb I would have
prefered to use `Thor` and `Aruba` libraries. Finally, I had in mind to use a 
data analysis library to optimize time-series data analysis.

I am happy with the simplicity of the API and having avoided managing threads
by using to the `eventmachine` library.


Installation
----

Tested only on Ruby 2.5.0

```
$ git clone git@gitlab.com:marconius/websocket-aggregator.git
$ cd websocket-aggrefator
$ bundle install
```


Usage
----

Example:

`ruby bin/start.rb ws://localhost:8080`
1

Running Test
----

```
$ bundle install --with test
$ rspecs
```
