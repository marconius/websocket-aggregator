require 'timecop'

require_relative '../lib/time_series'

describe 'TimeSeries' do
  subject { TimeSeries.new }

  it { expect(subject.instance_variable_get(:@data)).to eq([]) }

  describe 'can receive new data' do
    it { expect(subject).to respond_to(:record) }

    it 'stores data' do
      value = 2

      subject.record(value)

      expect(subject.instance_variable_get(:@data)[0][1]).to equal(value)
    end

    it 'marks data with timestamp' do
      new_time = Time.local(2008, 9, 1, 12, 0, 0)
      Timecop.freeze(new_time)
      value = 4

      subject.record(value)

      expect(subject.instance_variable_get(:@data)).to eq([[new_time, value]])
    end
  end

  describe 'averages' do
    let(:new_time) { Time.local(2008, 9, 1, 12, 0, 0) }

    before { Timecop.freeze(new_time) }

    it { expect(subject).to respond_to(:averages) }

    it 'values in the last minute' do
      subject.instance_variable_set :@data, [
        [new_time - 10, 2],
        [new_time, 2]
      ]

      expect(subject.averages).to eq([2, 2, 2])
    end

    it 'values in the last five minutes' do
      subject.instance_variable_set :@data, [
        [new_time - 220, 1],
        [new_time - 120, 4]
      ]

      expect(subject.averages).to eq([0, 2.5, 2.5])
    end

    it 'values in the last 15 minutes' do
      subject.instance_variable_set :@data, [
        [new_time - 850, 1],
        [new_time - 445, 3.5],
        [new_time - 350, 4.5]
      ]

      expect(subject.averages).to eq([0, 0, 3])
    end
  end

  describe '15 minute average' do
  end
end
