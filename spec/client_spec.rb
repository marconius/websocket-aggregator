require_relative '../lib/client'

describe 'Client' do
  let(:sampler) { instance_double('TimeSeries') }
  let(:uri) { 'localhost:8080' }

  subject { Client.new(uri, sampler) }

  before { allow(sampler).to receive(:averages).and_return('HELLO!') }

  it { expect(subject.instance_variable_get(:@uri)).to equal(uri) }
  it { expect(subject.instance_variable_get(:@data)).to equal(sampler) }

  it { expect(subject.averages).to equal(sampler.averages) }
end
