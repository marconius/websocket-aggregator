require_relative '../lib/client'
require_relative '../lib/time_series'

begin
  sampler = TimeSeries.new

  client = Client.new(ARGV[0], sampler)

  EM.run do
    EventMachine::PeriodicTimer.new(1) do
      averages = client.averages
      STDOUT.puts JSON.generate(
        '1' => averages[0], '5' => averages[1], '15' => averages[2]
      )
    end

    client.ws

    client.onmessage
  end
rescue Interrupt
  p 'Keyboard Interrupt'
end
